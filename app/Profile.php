<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use CustomModel, SoftDeletes;

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profile';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name',
        'm_name',
        'l_name',
        'birthdate'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function user()
    {
        //return $this->hasOne('App\User');

        //Defining the inversion of relation on the Profile model
        return $this->belongsTo('App\User');
    }

    public function getFullNameAttribute() {
        $format = '%s %s %s';
        return sprintf($format, $this->f_name, $this->m_name, $this->l_name);
    }

}
