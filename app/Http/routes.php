<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'IndexController@index');
Route::get('menu',['uses' => 'IndexController@menu', 'as' => 'index.menu']);
Route::get('home', 'HomeController@index');

Route::get('profile/view', ['middleware' => 'auth', 'uses' => 'ProfileController@viewProfile']);

Route::get('profile/view/{username}', 'ProfileController@viewUser');

Route::get('profile/edit', ['middleware' => 'auth', 'uses' => 'ProfileController@getEdit']);
Route::post('profile/edit', ['middleware' => 'auth', 'uses' => 'ProfileController@postEdit']);

Route::get('company/view/', ['middleware' => 'auth', 'uses' => 'CompanyController@viewMyCompany']);
Route::get('company/view/{id}', 'CompanyController@viewCompany');

Route::get('company/create', ['middleware' => 'auth', 'uses' => 'CompanyController@getCreate']);
Route::post('company/create', ['middleware' => 'auth', 'uses' => 'CompanyController@postCreate']);

Route::get('company/edit', ['middleware' => 'auth', 'uses' => 'CompanyController@getEdit']);
Route::post('company/edit', ['middleware' => 'auth', 'uses' => 'CompanyController@postEdit']);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('user/delete/{id}', ['middleware' => 'admin', 'uses' => 'UserController@getDelete']);
Route::get('user/active/{id}', ['middleware' => 'admin', 'uses' => 'UserController@getActive']);

Route::get('admin/accounts', 'AdminController@getAccounts');

Route::get('admin/company', ['middleware' => 'admin', 'uses' => 'AdminController@getCompanies', 'as' => 'company.index']);
Route::delete('admin/company/{id}', ['middleware' => 'admin', 'uses' => 'AdminController@destroyCompany', 'as' => 'company.destroy']);
Route::post('admin/company/{id}', ['middleware' => 'admin', 'uses' => 'AdminController@updateCompany', 'as' => 'company.update']);

Route::resource('city', 'CityController');
Route::resource('role', 'RoleController');
Route::resource('category', 'CategoryController');

Route::get('product/view/{id}', 'ProductController@viewProduct');

// Ratings
Route::post('rating/{product_id}', ['middleware' => 'auth', 'uses' => 'RatingController@create', 'as' => 'rating.create']);
Route::delete('rating/{rating_id}', ['middleware' => 'auth', 'uses' => 'RatingController@delete', 'as' => 'rating.delete']);

Route::get('product/create', ['middleware' => 'auth', 'uses' => 'ProductController@getCreate']);
Route::post('product/create', ['middleware' => 'auth', 'uses' => 'ProductController@postCreate']);

Route::get('product/edit/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@getEdit']);
Route::post('product/edit/', ['middleware' => 'auth', 'uses' => 'ProductController@postEdit']);
Route::get('/product/delete/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@getDelete']);
Route::delete('image/{id}', ['middleware' => 'auth', 'uses' => 'ImageController@destroy', 'as' => 'image.destroy']);
Route::get('product/', ['uses' => 'ProductController@index', 'as' => 'product.index']);

Route::get('/search', ['uses' => 'SearchController@index', 'as' => 'search.index']);
