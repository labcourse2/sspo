<?php namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Product;
use App\Tag;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $req)
	{
		$this->validate($req, [
             'query'  => 'required|min:3',
		]);
		$query = preg_replace('[%_]', '', strip_tags($req->input('query')));
		$products = Product::where('name', 'LIKE', "%$query%")->get();
		$categories = Category::where('name', 'LIKE', "%$query%")->get();
		$tags = Tag::where('name', 'LIKE', "%$query%")->get();
		$companies = Company::where('name', 'LIKE', "%$query%")->get();
		$users = User::where('username', 'LIKE', "%$query%")->get();

		$total = 	$products->count() +
					$categories->count() +
					$tags->count() +
					$companies->count() +
					$users->count();

		return view('search.index')->with([
			'products'=>$products,
			'categories'=>$categories,
			'tags'=>$tags,
			'companies'=>$companies,
			'users'=>$users,
			'total'=>$total,
		]);
	}

}