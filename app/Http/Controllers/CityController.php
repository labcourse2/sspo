<?php namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CityController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cities = City::all();
        return view('admin.city.list')->with(['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|unique:city,name'
        ]);
        $city = new City(['name' => $req->input('name')]);
        $city->save();
        Session::flash('message', 'City created successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/city');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $city = City::findById($id);
        if (!$city) {
            Session::flash('message', 'City with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/city');
        }
        return view('admin.city.edit')->with(['city' => $city]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'id' => 'integer'
        ]);
        $city = City::findById($id);
        if (!$city) {
            Session::flash('message', 'City with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/city');
        }
        $city->name = $req->input('name');
        $city->save();
        Session::flash('message', 'City updated successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/city');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $city = City::findById($id);
        if (!$city) {
            Session::flash('message', 'City with givbn id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/city');
        } else if ( !($city->companies->isEmpty() && $city->profiles->isEmpty()) ) {
            Session::flash('message', 'This city cannot be removed because there are companies and/or users associated with it!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/city');
        }
        $city->delete();
        Session::flash('message', 'City deleted successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/city');

    }

}
