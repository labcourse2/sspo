<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Welcome Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    private $rules = [
        'id' => 'required|integer',
        'f_name' => 'required|max:255',
        'm_name' => 'max:255',
        'l_name' => 'required|max:255',
        'city' => 'exists:city,id|required|integer'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function viewProfile() {
        $categories = Category::all();
        $cities = City::all();
        if (Auth::check()) {
            $user = Auth::getUser();
        } else {
            Session::flash('message', 'User not logged in!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        return view('profile.index')->with(['user' => $user, 'categories' => $categories, 'cities' => $cities]);
    }

    public function viewUser($username) {
        $categories = Category::all();
        $cities = City::all();
        $user = User::where('username', '=', $username)->first();
        if (!$user) {
            Session::flash('message', 'User with given username does not exists!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        return view('profile.index')->with(['user' => $user, 'categories' => $categories, 'cities' => $cities]);
    }

    public function getEdit() {
        $categories = Category::all();
        $cities = City::all();
        if (Auth::check()) {
            $user = Auth::getUser();
        } else {
            Session::flash('message', 'User not logged in!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        return view('profile.edit')->with(['user' => $user, 'categories' => $categories, 'cities' => $cities]);
    }

    private function updateProfile(Profile $profile, Request $req) {
        $profile->f_name = $req->input('f_name');
        $profile->m_name = $req->input('m_name');
        $profile->l_name = $req->input('l_name');
        $profile->birthdate = $req->input('birthdate');
        $city = City::findById($req->input('city'));
        $profile->city()->associate($city);
        $profile->save();
        return $profile;
    }

    public function postEdit(Request $req) {
        $this->validate($req, $this->rules);
        $user = User::findById($req->input('id'));
        $this->updateProfile($user->profile()->first(), $req);
        return redirect('profile/edit')->withErrors(['Error message goes here']);
    }

}
