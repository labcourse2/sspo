<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Company;
use App\Telephone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CompanyController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Welcome Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function viewCompany($id) {
        $categories = Category::all();
        $cities = City::all();
        $company = Company::where('id', '=', $id)->first();
        if (!$company) {
            Session::flash('message', 'Company with selected id does not exists');
            Session::flash('alert-class', 'alert-info');
            return redirect('/');
        }
        return view('company.index')->with(['company' => $company, 'categories' => $categories, 'cities' => $cities]);
    }

    public function viewMyCompany() {
        $categories = Category::all();
        $cities = City::all();

        if (Auth::check()) {
            $user = Auth::getUser();
        }
        $company = $user->company()->first();

        return view('company.index')->with(['company' => $company, 'categories' => $categories, 'cities' => $cities]);
    }

    public function getCreate() {
        $categories = Category::all();
        $cities = City::all();

        if (Auth::check()) {
            $user = Auth::getUser();
        }
        $has_company = $user->company()->first();
        if ($has_company) {
            Session::flash('message', 'You already have created a company, only one company per user is allowed!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('company/view');
        }
        return view('company.create')->with(['user' => $user, 'categories' => $categories, 'cities' => $cities]);
    }

    private function uploadImage($file)
    {
        $destinationPath = public_path() . getenv('UPLOADS_DIR');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
        $file->move($destinationPath, $fileName);
        return $fileName;
    }

    private function sanitize($dirty) {
        $clean = preg_replace ("/ +/", " ", $dirty); # convert all multispaces to space
        $clean - preg_replace ('/-+/', '-', $clean); # convert all multi dashes to one dash
        $clean = trim($clean, " ");
        $clean = trim($clean, "-");
        return $clean;
    }

    public function postCreate(Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'email' => 'unique:company|required|email|max:255',
            'slogan'=>"max:255",
            'web_address' => 'required|url|max:255',
            'address' => 'required|max:255',
            'telephone' => 'required|regex:/^[0-9 -]+$/',
            'city'=> 'required|integer|exists:city,id',
            'logo_image'=> 'required|mimes:jpeg,bmp,png'
        ]);

        if (Auth::check()) {
            $user = Auth::getUser();
        }
        $has_company = $user->company()->first();
        if ($has_company) {
            Session::flash('message', 'You already have created a company, only one company per user is allowed!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('company/view');
        }

        $city_id = $req->input('city');
        $city = City::findById($city_id);
        $company = new Company([
            'name' => $req->input('name'),
            'address'=>$req->input('address'),
            'email' => $req->input('email'),
            'slogan' => $req->input('slogan'),
            'web_address' => $req->input('web_address'),
            'description'=>$req->input('description'),
            'telephone'=>$this->sanitize($req->input('telephone')),
            'logo_image' => $this->uploadImage($req->file('logo_image'))
        ]);
        $company->owner()->associate($user);
        $company->city()->associate($city);
        $company->save();

        return redirect('company/view');
    }

    public function getEdit() {

        $categories = Category::all();
        $cities = City::all();

        if (Auth::check()) {
            $user = Auth::getUser();
        } else {
            return "User not logged in!";
        }
        $company = $user->company()->first();

        return view('company.edit')->with(['company' => $company, 'user' => $user, 'categories' => $categories, 'cities' => $cities]);
    }

    public function postEdit(Request $req) {

        $this->validate($req, [
            'name' => 'required',
            'email' => 'required|email|max:255',
            'slogan'=>"max:255",
            'web_address' => 'required|url|max:255',
            'address' => 'required|max:255',
            'telephone' => 'required|regex:/^[0-9 -]+$/',
            'city'=> 'required|integer|exists:city,id',
            'logo_image'=>'mimes:jpeg,png,bmp'
        ]);

        $company_id = $req->input('company_id');
        $company = Company::findById($company_id);

        $company->name = $req->input('name');
        $company->email = $req->input('email');
        $company->web_address = $req->input('web_address');
        $company->address = $req->input('address');
        $company->slogan = $req->input('slogan');
        $company->description = $req->input('description');
        $company->telephone = $this->sanitize($req->input('telephone'));
        $file = $req->file('logo_image');
        if ($file && $file->isValid()) {
            $company->logo_image = $this->uploadImage($file);
        }
        $city_id = $req->input('city');
        $city = City::findById($city_id);
        $company->city()->associate($city);
        $company->save();

        Session::flash('message', 'Changes saved succsesfully');
        Session::flash('alert-class', 'alert-success');
        return redirect('company/edit');
    }

}
