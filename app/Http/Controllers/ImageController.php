<?php namespace App\Http\Controllers;

use App\Image;
use Illuminate\Support\Facades\File;

class ImageController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$image = Image::findById($id);
		$image->delete();
		return (File::delete(public_path() . $image->url))? 'Success' : "Fail";
	}

}
