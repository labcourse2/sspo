<?php namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function getAccounts()
    {
        $users = User::all();
        return view('admin/accounts')->with(['users' => $users]);
    }

    public function getCompanies()
    {
        $companies = Company::all();
        return view('admin/company/list')->with(['companies' => $companies]);
    }

    public function updateCompany($id, Request $req)
    {

        $this->validate($req, [
            'featured' => 'integer|in:0,1'
        ]);

        $company = Company::findById($id);
        if (!$company) {
            Session::flash('message', 'Company with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/admin/company');
        }
        $company->featured = $req->input('featured');
        $company->save();
        Session::flash('message', 'Company updated successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/company');

    }

    public function destroyCompany($id) {
        $company = Company::findById($id);
        if (!$company) {
            Session::flash('message', 'Company with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/admin/company');
        }
        $company->delete();
        Session::flash('message', 'Company deleted successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/admin/company');

    }
}
