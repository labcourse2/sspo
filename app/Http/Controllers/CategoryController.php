<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.category.list')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|unique:category,name'
        ]);
        $cat = new Category([
            'name' => $req->input('name')
        ]);
        $cat->save();
        Session::flash('message', 'Category created successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $cat = Category::findById($id);
        if (!$cat) {
            Session::flash('message', 'Category with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/category');
        }
        return view('admin.category.edit')->with(['category' => $cat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'id' => 'integer'
        ]);
        $cat = Category::findById($id);
        if (!$cat) {
            Session::flash('message', 'Category with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/category');
        }
        $cat->name = $req->input('name');
        $cat->save();
        Session::flash('message', 'Category updated successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $cat = Category::findById($id);
        if (!$cat) {
            Session::flash('message', 'Category with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/category');
        } else if ($cat->products()->count() != 0) {
            Session::flash('message', 'This category cannot be removed because there are products categorized in it!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/category');
        }
        $cat->delete();
        Session::flash('message', 'Category deleted successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/category');
    }

}
