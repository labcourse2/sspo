<?php namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $categories = Category::all()->take(5);
		return view('index')->with(['categories'=>$categories]);
    }

	public function getDelete($id)
	{
		$user = User::findById($id);
		if (!$user) {
			Session::flash('message', 'User account with given id does not exist!');
			Session::flash('alert-class', 'alert-danger');
			return redirect('/admin/accounts');
		}
		if (Auth::check() && ($user->id == Auth::getUser()->id)) {
			Session::flash('message', 'You cannot delete your own user account!');
			Session::flash('alert-class', 'alert-danger');
			return redirect('/admin/accounts');
		}
		$user->delete();
		Session::flash('message', 'User account deleted successfully!');
		Session::flash('alert-class', 'alert-success');
		return redirect('/admin/accounts');
	}

	public function getActive($id, Request $req)
	{
		$this->validate($req, [
			'active' => 'required|in:1,0'
		]);
		$user = User::findById($id);
		if (!$user) {
			Session::flash('message', 'User account does not exist!');
			Session::flash('alert-class', 'alert-danger');
			return redirect('/admin/accounts');
		}
		if (Auth::check() && ($user->id == Auth::getUser()->id)) {
			Session::flash('message', 'You cannot edit the status of your own user account!');
			Session::flash('alert-class', 'alert-danger');
			return redirect('/admin/accounts');
		}
		$user->active = $req->input('active');
		$user->save();
		Session::flash('message', 'User account updated successfully!');
		Session::flash('alert-class', 'alert-success');
		return redirect('/admin/accounts');

	}
}
