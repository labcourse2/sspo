<?php namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Product;
use Illuminate\Support\Facades\Auth;


class IndexController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $categories = Category::all()->take();
        $company = Company::where('featured', '=', '1')->first();

        return view('index')->with(['categories' => $categories, 'company' => $company]);
    }

	public function menu() {
		$categories = Category::all();

		return view('parts.menu')->with(['categories'=>$categories]);
	}
}
