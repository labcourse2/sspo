<?php namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('admin.role.list')->with(['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|min:3|alpha_dash|unique:role,name',
        ]);
        $role = new Role([
            'name' => $req->input('name'),
            'admin' => $req->input('admin', 0)
        ]);
        $role->save();
        Session::flash('message', 'Role created successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $role = Role::findById($id);
        if (!$role) {
            Session::flash('message', 'Role with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/role');
        }
        return view('admin.role.edit')->with(['role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $req)
    {
        $this->validate($req, [
            'name' => 'required|min:3|alpha_dash',
            'id' => 'integer'
        ]);
        $role = Role::findById($id);
        if (!$role) {
            Session::flash('message', 'Role with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/role');
        }
        $role->name = $req->input('name');
        $role->admin = $req->input('admin', 0);
        $role->save();
        Session::flash('message', 'Role updated successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $role = Role::findById($id);
        if (!$role) {
            Session::flash('message', 'Role with given id does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/role');
        } else if ($role->users()->count() != 0) {
            Session::flash('message', 'This Role cannot be removed because there are user accounts associated with it. Delete the accounts first!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/role');
        }
        $role->delete();
        Session::flash('message', 'Role deleted successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/role');

    }

}
