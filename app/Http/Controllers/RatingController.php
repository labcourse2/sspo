<?php

namespace App\Http\Controllers;

 use App\Product;
 use App\Rating;
 use App\User;
 use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Auth;
 use Illuminate\Support\Facades\Session;

 class RatingController extends Controller {

     public function __construct()
     {
         //$this->middleware('guest');
     }

     public function create($product_id, Request $req) {

         $this->validate($req, [
             'rating'  => 'required',
             'comment' => 'required'
         ]);

         if(Auth::check()) {
             $user = Auth::getUser();
         }

         $product = Product::findById($product_id);

         $rating = $req->input('rating');
         $comment = $req->input('comment');

         $ratings = new Rating(['comment' => $comment, 'rating' => $rating]);
         $ratings->product()->associate($product);
         $ratings->user()->associate($user);

         $ratings->save();
         Session::flash('message', 'Your comment and rating was added successfully to the product page.');
         Session::flash('alert-class', 'alert-success');
         return redirect('product/view/' . $product->id);
     }

     public function delete($rating_id) {

         $rating =Rating::findById($rating_id);

         if (!$rating) {
            Session::flash('message', 'The selected rating and comment does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }

         $rating->delete();

         Session::flash('message', 'Your rating and comment was deleted successfully');
         Session::flash('alert-class', 'alert-success');
         return redirect('product/view/'.$rating->product()->first()->id);
     }
 }