<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Gallery;
use App\Image;
use App\Price;
use App\Product;
use App\Rating;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Welcome Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function index(Request $req) {
        if ($req->has('category')) {
            $cat_id = $req->input('category');
            $category = Category::findById($cat_id);
            $result = $category;
        } else if ($req->has('tag')) {
            $tag_id = $req->input('tag');
            $tag = Tag::findById($tag_id);
            $result = $tag;
        } else {
            Session::flash('message', 'Please specify a valid category or tag.');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        if (!$result) {
            Session::flash('message', 'The specified category or tag does not exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        if (!$result->products || $result->products->isEmpty()) {
            Session::flash('message', 'There are no products in the "' . $result->name .'" ' . $result->getTable() . '!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        return view('product.list')->with(['result' => $result]);
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function viewProduct($id)
    {
        $categories = Category::all();
        $ratings = Rating::all();
        $product = Product::findById($id);

        if (!$product) {
            Session::flash('message', 'Product with selected id does not exists');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        return view('product.index')->with(['product' => $product, 'categories' => $categories, 'ratings' => $ratings]);
    }

    public function getCreate()
    {
        $categories = Category::all();
        $products = Product::all();
        $prices = Price::all();

        if (Auth::check()) {
            $user = Auth::getUser();
        }
        $company = $user->company()->first();
        if (!$company) {
            Session::flash('message', 'You need a company to add products');
            Session::flash('alert-class', 'alert-info');
            return redirect('/company/create');
        }
        return view('product.create')->with(['user' => $user, 'categories' => $categories, 'products' => $products, 'prices' => $prices]);
    }

    private function uploadImage($file, Product $product)
    {
        $destinationPath = public_path() . getenv('UPLOADS_DIR') . getenv('PRODUCTS_IMAGES_DIR') . $product->id . '/';
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $fileName = md5($product->name) . rand(11111, 99999) . '.' . $extension; // renaming image
        $file->move($destinationPath, $fileName);
        return $fileName;
    }
    
    public function create_tags($tags_str) {
        $tags = explode(",",$tags_str);
        $res = [];
        foreach ($tags as $tag) {
            $res[] = Tag::firstOrCreate(['name'=>Str::lower($tag)]);
        }
        return $res;
    }

    public function postCreate(Request $req)
    {

        $this->validate($req, [
            'name' => 'required',
            'price' => 'required|numeric',
            'sh_description' => 'required',
            'l_description' => 'required',
            'sh_description' => 'required',
            'tags'=>'required'
        ]);
        
        $tags = $this->create_tags($req->input('tags'));
        $company_id = $req->input('company');
        $company = Company::findById($company_id);

        $category_id = $req->input('category');
        $category = Category::findById($category_id);

        $product = new Product([
            'name' => $req->input('name'),
            'sh_description' => $req->input('sh_description'),
            'l_description' => $req->input('l_description'),
            ]);
        $product->prices();
        $product->company()->associate($company);
        $product->category()->associate($category);
        $product->save();
        foreach ($tags as $tag) {
            $product->tags()->attach($tag);
        }
        $product->push();

        $price = new Price(['price' => $req->input('price'), 'active' => 1]);
        $price->product()->associate($product);
        $price->save();

        $files = $req->file('images');
        $files_count = count($files);
        foreach ($files as $file) {
            if (!empty($file) && $file->isValid()) {
                $image = new Image([
                    'path' => $this->uploadImage($file, $product)
                ]);
                $image->product()->associate($product);
                $image->save();
            }
        }
        Session::flash('message', 'Product created with success');
        Session::flash('alert-class', 'alert-success');
        return redirect('product/view/' . $product->id);
    }

    public function getEdit($id)
    {
        $categories = Category::all();
        $product = Product::findById($id);
        if (Auth::check()) {
            $user = Auth::getUser();
        }
        if (!$product->ownedBy($user)) {
            Session::flash('message', 'You are not allowed to edit this product!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        return view('product.edit')->with(['categories' => $categories, 'product' => $product]);

    }

    public function postEdit(Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'price' => 'required|numeric',
            'sh_description' => 'required',
            'l_description' => 'required',
            'sh_description' => 'required',
        ]);

        $id = intval($req->input('id'));
        $product = Product::findById($id);
        if (Auth::check()) {
            $user = Auth::getUser();
        }
        if (!$product->ownedBy($user)) {
            Session::flash('message', 'You are not allowed to edit this product!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        $product->name = $req->input('name');
        $product->sh_description = $req->input('sh_description');
        $product->l_description = $req->input('l_description');

        $old_tags = $product->tags()->get();
        foreach ($old_tags as $old_tag) {
            $product->tags()->detach($old_tag);
        }
        $product->push();
        $tags = $this->create_tags($req->input('tags'));
        foreach ($tags as $tag) {
            $product->tags()->attach($tag);
        }
        $product->push();
        
        
        $price_val = $req->input('price');
        $price = $product->prices()->first();
        $price->price = $price_val;
        $price->save();

        $category_id = $req->input('category');
        $category = Category::where('id', '=', $category_id)->first();

        $product->category()->associate($category);

        $product->save();
        foreach ($product->images() as $image) {
            $image->delete();
        }
        $files = $req->file('images');
        $files_count = count($files);
        foreach ($files as $file) {
            if (!empty($file) && $file->isValid()) {
                $image = new Image([
                    'path' => $this->uploadImage($file, $product)
                ]);
                $image->product()->associate($product);
                $image->save();
            }
        }
        Session::flash('message', 'Product edited with success');
        Session::flash('alert-class', 'alert-success');
        return redirect('product/view/' . $product->id);
    }

    public function getDelete($id)
    {
        $product = Product::findById($id);
        if (Auth::check()) {
            $user = Auth::getUser();
        }
        if (!$product->ownedBy($user)) {
            Session::flash('message', 'You are not allowed to delete this product!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/');
        }
        $product->delete();
        Session::flash('message', 'Product was deleted with success');
        Session::flash('alert-class', 'alert-success');
        return redirect('/company/view/');
    }
}
