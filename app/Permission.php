<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    use CustomModel;
    public $timestamps = false;
    protected $table = 'permission';

    //Blocking all attributes from 'mass assignment'
    protected $guarded = ['*'];

    public function role()
    {
        //Mapped tables (permission & role <=> many to many relationship)
        return $this->belongsToMany('App\Role', 'role_id', 'id');
    }
}
