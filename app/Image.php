<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Image extends Model
{

    use CustomModel;

    public $timestamps = false;
    protected $table = 'image';
    protected $fillable = ['path'];
    protected $hidden = [];

    public function getUrlAttribute()
    {
        $destinationPath = getenv('UPLOADS_DIR') . getenv('PRODUCTS_IMAGES_DIR') . $this->product()->first()->id . '/';
        return $destinationPath . $this->path;
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function deleteFromDisk() {
        $file = public_path() . getenv('UPLOADS_DIR') . getenv('PRODUCTS_IMAGES_DIR') . $this->product()->first()->id . '/' . $this->path;
        return $file;
        if (File::isFile($file)) {
            File::delete();
        }
    }

}
