<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    use CustomModel;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'message';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['read', 'subject', 'content'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function from()
    {
        return $this->belongsTo('App\User', 'from');
    }

    public function to()
    {
        return $this->belongsTo('App\User', 'to');
    }

}
