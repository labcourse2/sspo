<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    use CustomModel;

    public $timestamps = false;
    protected $table = 'role';
    protected $fillable = ['name', 'admin'];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public static function findAdmin()
    {
        return static::where('admin', '=', '1')->first();
    }

    public function permission()
    {
        //Mapped tables (role & permission <=> many to many relationship)
        return $this->belongsToMany('App\Permission');
    }
}
