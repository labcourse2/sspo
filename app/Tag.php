<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use CustomModel;

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tag';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function products() {
        return $this->belongsToMany('App\Product', 'product_has_tag');
    }
}
