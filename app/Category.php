<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use CustomModel, SoftDeletes;

    protected $table = 'category';

    protected $fillable = ['name'];

    protected $hidden = [];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function productsFor($company)
    {
        $products = $this->products()->where('company_id', '=', $company->id);
        return $products->get();
    }
}
