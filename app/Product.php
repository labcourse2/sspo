<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use CustomModel;

    use SoftDeletes;

    protected $table = 'product';

    protected $fillable = ['name', 'sh_description', 'l_description'];

    protected $hidden = [];

    public function delete() {
        //delete all associated prices
        $this->prices()->delete();
        //delete all associated ratings
        $this->ratings()->delete();
        //delete all images
        $this->images()->delete();
        parent::delete();
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function prices()
    {
        return $this->hasMany('App\Price');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }

    //Mapped tables (product & tag <=> many to many relationship)
    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'product_has_tag');
    }

    public function getActivePriceAttributes() {
        return $this->prices()->where('active', '=', 1)->first();
    }
    
    public function ownedBy(User $user) {
        $usr_comp = $user->company()->first();
        $prd_comp =  $this->company()->first();
        return ($usr_comp && $usr_comp->id === $prd_comp->id);
    }
    
    public function getShowTagsAttribute() {
        $tags = $this->tags()->get();
        $res = '';
        foreach ($tags as $tag) {
            $res .= $tag->name . ',';
        }
        return $res;
    }
}
