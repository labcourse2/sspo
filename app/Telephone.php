<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Telephone extends Model
{
    use CustomModel;

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'telephone';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['home', 'work', 'viber', 'skype', 'whatsapp'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function company() {
        return $this->belongsTo('App\Company');
    }

}
