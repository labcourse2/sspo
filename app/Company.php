<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;

class Company extends Model
{
    use CustomModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address', 'logo_image', 'web_address', 'slogan',
        'email', 'description', 'telephone'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function delete()
    {
        // delete all associated products
        $this->products()->delete();
        // delete image
        $image = public_path() . $this->getLogoImageLinkAttribute();
        if (File::isFile($image)) {
            File::delete($image);
        }
        parent::delete();

    }


    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function isOwner(User $user)
    {
        return ($this->owner()->first()->id === $user->id);
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function getLogoImageLinkAttribute()
    {
        return getenv('UPLOADS_DIR') . $this->logo_image;
    }
}
