<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

trait CustomModel {

    /**
     * @param $id
     * @param string $primaryKey
     * @param string $columns
     * @return Model
     */
    public static function findById($id, $primaryKey='id', $columns = '*') {
        return parent::where($primaryKey, '=', intval($id))->first();
    }

}
