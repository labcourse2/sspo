<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{

    use CustomModel;
    public $timestamps = false;
    protected $table = 'price_history';
    protected $fillable = ['price', 'description', 'active'];

    protected $hidden = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function active() {
        $this->getAttributeValue('active');
    }


}
