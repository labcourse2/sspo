<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword, CustomModel, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'username', 'password', 'active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function delete()
    {
        // delete associated profile
        $this->profile()->delete();
        // delete associated company
        $this->company()->delete();

        //delete all associated ratings
        $this->ratings()->delete();

        // delete the user
        return parent::delete();
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function company()
    {
        return $this->hasOne('App\Company');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }

    public function ratingFor($product) {
        $ratings = $this->ratings();
        $rating = $ratings->where('product_id', '=', $product)->first();
        return $rating;
    }

    public function message()
    {
        return $this->hasMany('App\Message');
    }

    public function getGravatarAttribute() {
        $hash = md5(strtolower(trim($this->attributes['email'])));
        $size = 400;
        return "http://gravatar.com/avatar/$hash". "?&s=" . $size;
    }

    public function isAdmin() {
        return $this->role()->first() == Role::findAdmin();
    }

    
}
