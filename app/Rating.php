<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    use CustomModel;

    protected $table = 'rating';

    protected $fillable = ['rating', 'comment'];

    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
