<?php namespace App\Services;

use App\User;
use App\Role;
use App\Profile;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:user',
            'email' => 'required|email|max:255|unique:user',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    public function create(array $data)
    {
        $user = new User([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $default_role = $this->createMandatoryRoles('admin', 'default');
        $user->role()->associate($default_role);
        $user->save();
        $profile = new Profile();
        $profile->user()->associate($user);
        $profile->save();
        return $user;

    }

    public function createMandatoryRoles($admin_n, $default_n) {
        Role::firstOrCreate(array('name' => $admin_n, 'admin'=>true));
        return $default_role = Role::firstOrCreate(array('name' => $default_n));
    }

}
