# Detyrat per zhvillimin e SSPO

1. Shkrimi i gjitha Modeleve
    1. Te gjitha tabelat duhet te reprezentohen si klasa ne kod, bashk me relacionet e tyre.

2. Regjistrimi
    1. Vetem email dhe password
    2. Konfirmimi i email - NOT IMPLEMENTED

3. Kyqja
    1. Div me klasen "login-register-search" duhet te ndryshoj pamjen vareisht se ne eshte i kycur perdoruesi apo jo
    2. Forgot password

4. Konfirugimi i routave
    1. Te vendosen te gjitha rutat

5. Menyja
    1. Shfaqja e kategorive

6. Home page
    1. Shfaqja e produkteve te feature company
    2. Listimi i disa kategorive dhe produkte nga to

7. Profili
    1. Shfaqja e profilit
    2. Ndryshimi i proflit
    3. Ngarkimi i fotos

8. Kompania
    1. Regjistrimi i kompanise
    2. Ngarkimi i fotos
    3. Ndryshimi i kompanise

9. Produktet
    1. Krijimi i produktit
        1. Galeria e fotove
    2. Ndryshimi i produktit
    3. Fshirja e produktit

10. Rating & Commenting
    1. Shfaqja e vleresimit tek nje produkt (yllat.
    2. Vleresimi i nje produkti
    3. Komentimi i nje produkti

11. Messaging
    1. Shkrmi i nje mesazhi privat
    2. Njoftimi per mesazh
    3. Leximi i mesazhit
    4. Shkrimi ne forme pergjigje i mesazhit
    5. Listimi i mesazheve te derguara
    6. Listimi i mesazheve te pranuara
    7. Shkrimi i nje mesazhi privat nga nje produkt
        1. Produkti do te ceket ne subjekt

12. Admin
    1. Listimi i perdoruese
        1. Informata te shkurta mbi ta
        2. Percaktimi i nje kompanie si 'featured'

## Products hapat e zhvillimit

1. Krijo ProductController ( U kry)
  
  Krijo keto routa me metoda

2. GET /products/ - getList() 
  
  Nese shfrytezuesi ka kompani -> shfaq listen e produkteve sipas slice
  Nese shfrytezuesi **nuk** ka kompani -> redirect tek forma per krijim kompanie me mesazhin "Nuk keni kompani. Ju duhet nje kompani" ( apo te ngjashme)

3. GET /products/create - getCreate()
  
  Shfaq formen per detajet e produktit te ri

4. POST /products/create postCreate()
  
  Ketu behet submit forma dhe logjika me ane se ciles produkti regjistrohet ne DB.
  
  $product = new Product(.....)
  //... beni assign vlerat me atribute,
  //... asocio produktin tek kompanai e userit te kyqyr (Auth::getUser)
  $product->save()

5. GET /products/edit/{id} getEdit()
  
  Shfaq formen me ane se ciles ndryshohen te dhenat e produktit ky produkt
 
6. POST /products/edit/{id} postEdit()
  
  Ruaj ndryshimet . (shiqo piken 4)

7. DELETE /products/delete/{id} deleteProduct()
  
  Fshij produktin me softDelete()
  
