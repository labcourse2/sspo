(function() {

    // prevent default action of anchor tags
    $("a.trigger").on("click",function(event){
        event.preventDefault();
    });

    // login + register js
    var dlgtrigger = document.querySelectorAll('[data-dialog]'),
        somedialog = null,
        dlg = null;

    for(var i = 0; i < 2; i++) {
        somedialog = document.getElementById(dlgtrigger[i].getAttribute('data-dialog')),
        dlg = new DialogFx(somedialog),
        console.log(dlg),
        dlgtrigger[i].addEventListener('click',dlg.toggle.bind(dlg));
    }
    
    //OWL SLIDER
    var owl = $("#owl-demo");

          owl.owlCarousel({
            items : 4
          });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      });
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      });
    
    // Validate
    $(".form-horizontal").validate();
    $(".validate-form").validate();
    
     $("[data-toggle=popover]")
.popover({html:true,
         trigger: "hover focus"})
    
})();


(function(){
    $('ul.grandchild-list').hide();

    $('ul.parent-list > li > a').on('click',function(){
         
        var $this = $(this).parent("li");
        
        
        $this.children('ul.grandchild-list').slideToggle(400);
        
        $this.siblings().children('ul.grandchild-list').hide();

    });
    
})();