(function() {
$( "#datepicker" ).datepicker({
     dateFormat: 'yy-mm-dd'
    });
    // prevent default action of anchor tags
    $("a.trigger").on("click",function(event){
        event.preventDefault();
    });

    // login + register js
    var dlgtrigger = document.querySelectorAll('[data-dialog]'),
        somedialog = null,
        dlg = null;
    
    //OWL SLIDER
    var owl = $("#owl-demo");

          owl.owlCarousel({
            items : 4
          });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      });
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      });
    
    // Validate
    //$(".form-horizontal").validate();
    $(".validate-form").validate();
    
     $("[data-toggle=popover]")
.popover({html:true,
         trigger: "hover focus"})
    
})();


(function(){
    $('ul.grandchild-list').hide();

    $('ul.parent-list > li > a').on('click',function(){
         
        var $this = $(this).parent("li");
        
        
        $this.children('ul.grandchild-list').slideToggle(400);
        
        $this.siblings().children('ul.grandchild-list').hide();

    });
    
})();

$(document).ready(function() {
    $(".navbar.navbar-default").load("/menu");
    $('.table').DataTable();
    $('.product-gallery').bxSlider({
        adaptiveHeight: true
    });
    $('.js-rating').rating();
    
    $('.remove-img').on('click', function(e) {
        e.preventDefault();
        $(this).parent().remove();
    });
    
    $('.delete-confirm').on('click', function(e) {
        e.preventDefault();
        var submitForm = $(this).parent();
        
        bootbox.confirm("Are you sure you want to delete?<br/>This action cannot be undone!?", function (result) {
            if (result) {
                submitForm.submit();
                return true;
            } 
        });
    });
    
    
    $('.delete-user').on('click', function(e) {
        e.preventDefault();
        var hrefAttr = $(this).attr('href');
        
         bootbox.confirm("Are you sure you want to delete?<br/>This action cannot be undone!", function (result) {
            if (result) {
                document.location.href = hrefAttr;
                return true;
            } 
        });
    });
});
