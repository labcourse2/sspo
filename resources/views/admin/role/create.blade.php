@extends('layouts.master')

@section('title')
Roles

@stop

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>There were some problems with your input.</strong><br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

{!! Form::open(['route' => ['role.store'], 'method' => 'post']) !!}
<div class="form-group">
    <div class="form-group">
        <label for="name">Role Name</label>
        {!! Form::text('name','', array('class'=>'form-control', 'id'=>'name')) !!}
    </div>

    <div class="form-group">
        <label for="name">Admin role:</label>
        {!! Form::checkbox('admin', 1) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Save', array('class'=>'btn btn-default')) !!}
    </div>

</div>
{!! Form::close() !!}
@stop