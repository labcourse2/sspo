@extends('layouts.master')

@section('title')
Roles

@stop

@section('content')

<a href="/role/create/" title="Add">
    <button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add
    </button>
</a>
<table class="table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Admin</th>
        <th>Users</th>
        <th>Actions</th>

    </tr>
    </thead>
    <tbody>
    @foreach ($roles as $role)
    <tr>
        <td>{{ $role->id }}</td>
        <td><strong>{{ $role->name }}</strong></td>
        <td>{{ ($role->admin == 1)? "Yes" : "No" }}</td>
        <td>{{ $role->users->count() }}</td>
        <td>
            {!! Form::open(['route' => ['role.destroy', $role->id], 'method' => 'delete']) !!}
            <button type="submit" class="btn btn-default delete-confirm">
                <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}

            {!! Form::open(['route' => ['role.edit', $role->id], 'method' => 'get']) !!}
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop