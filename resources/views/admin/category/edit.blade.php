@extends('layouts.master')

@section('title')
Edit Category

@stop

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>There were some problems with your input.</strong><br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

{!! Form::open(['route' => ['category.update', $category->id], 'method' => 'put']) !!}
<div class="form-group">
    <label for="name">Category name</label>
    {!! Form::text('name',$category->name, array('class'=>'form-control', 'id'=>'name')) !!}
    {!! Form::submit('Save', array('class'=>'btn btn-default')) !!}
</div>
{!! Form::close() !!}
@stop