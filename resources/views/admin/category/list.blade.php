@extends('layouts.master')

@section('title')
Cities

@stop

@section('content')

<a href="/category/create/" title="Add">
    <button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add
    </button>
</a>
<table class="table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Products</th>
        <th>Actions</th>

    </tr>
    </thead>
    <tbody>
    @foreach ($categories as $category)
    <tr>
        <td>{{ $category->id }}</td>
        <td><strong>{{ $category->name }}</strong></td>
        <td>{{ $category->products->count() }}</td>
        <td>
            {!! Form::open(['route' => ['category.destroy', $category->id], 'method' => 'delete']) !!}
            <button type="submit" class="btn btn-default delete-confirm">
                <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}

            {!! Form::open(['route' => ['category.edit', $category->id], 'method' => 'get']) !!}
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop