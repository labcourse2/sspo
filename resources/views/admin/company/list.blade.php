@extends('layouts.master')

@section('title')
Companies

@stop

@section('content')

<table class="table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Owner</th>
        <th>Email</th>
        <th>Web Site</th>
        <th>Created</th>
        <th>Products</th>
        <th>Actions</th>

    </tr>
    </thead>
    <tbody>
    @foreach ($companies as $company)
    <tr>
        <td>{{ $company->id }}</td>
        <td>
            <a href="/company/view/{{ $company->id }}">
                {{ $company->name }}
            </a>
        </td>
        <td>{{ $company->owner->username }}</td>
        <td><a href="mailto:{{ $company->email }}">{{ $company->email }}</a></td>
        <td>
            <a href="{{ $company->web_address }}">
                {{ $company->web_address }}
            </a>
        </td>
        <td>{{ date("d.m.Y",strtotime($company->created_at)) }}</td>
        <td>{{ $company->products->count() }}</td>
        <td>
            {!! Form::open(['route' => ['company.destroy', $company->id], 'method' => 'delete']) !!}
            <button type="submit" class="btn btn-default delete-confirm" title="Delete this company">
                <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}

            {!! Form::open(['route' => ['company.update', $company->id], 'method' => 'post']) !!}
            {!! ($company->featured)? Form::hidden('featured', 0) : Form::hidden('featured', 1) !!}
            <button type="submit" class="btn btn-default" title="{{ ($company->featured)? 'Remove' :'Make'}} featured">
                <span class="glyphicon glyphicon glyphicon-star {{ ($company->featured)? 'active' :''}}" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop