@extends('layouts.master')

@section('title')
Accounts

@stop

@section('content')

<table class="table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Email</th>
        <th>Full name</th>
        <th>Active</th>
        <th>Company</th>
        <th>Actions</th>

    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
    <tr>
        <td>{{ $user->id }}</td>
        <td><strong>{{ $user->username }}</strong></td>
        <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
        <td>{{ $user->profile->full_name }}</td>
        <td>{{ ($user->active === 1)? "Yes" : "No" }}</td>
        <td>{{ ($user->company)? '"'.$user->company->name.'"' : "N/A" }}</td>
        <td>
            <a href="{{ url('/user/delete/' . $user->id) }}" title="Delete" class="delete-user">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
            </a>

            @if ($user->active == 1)
            <a href="{{ url('/user/active/' . $user->id . '?active=0') }}" title="Disable">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </a>
            @else
            <a href="{{ url('/user/active/' . $user->id . '?active=1') }}" title="Enable">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                </button>
            </a>
            @endif
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop