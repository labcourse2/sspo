@extends('layouts.master')

@section('title')
Cities

@stop

@section('content')

<a href="/city/create/" title="Add">
    <button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add
    </button>
</a>
<table class="table">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Actions</th>

    </tr>
    </thead>
    <tbody>
    @foreach ($cities as $city)
    <tr>
        <td>{{ $city->id }}</td>
        <td><strong>{{ $city->name }}</strong></td>
        <td>
            {!! Form::open(['route' => ['city.destroy', $city->id], 'method' => 'delete']) !!}
            <button type="submit" class="btn btn-default delete-confirm">
                <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}

            {!! Form::open(['route' => ['city.edit', $city->id], 'method' => 'get']) !!}
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </button>
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop