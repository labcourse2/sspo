@extends('layouts.master')

@section('title', 'Login')

@section('content')

<div class="authenticate-form col-lg-5">
    <h2><strong>Register</strong></h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>There were some problems with your input.</strong><br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form class="form-horizontal" method="POST" action="{{ url('/auth/register') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">*Username</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="username"
                       value="{{ old('username') }}" placeholder="username" required>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">*Email</label>

            <div class="col-sm-10">
                <input type="email" class="form-control" name="email"
                       value="{{ old('email') }}" id="email" placeholder="account@email.com" required>
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">*Password</label>

            <div class="col-sm-10">
                <input type="password" class="form-control" name="password"
                       id="password" placeholder="********" required>
            </div>
        </div>

        <div class="form-group">
            <label for="password_confirm" class="col-sm-2 control-label">*Confirm Password</label>

            <div class="col-sm-10">
                <input type="password" class="form-control" name="password_confirmation"
                       id="password_confirm" placeholder="********" required>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Register</button>
            </div>
        </div>
    </form>
</div>
@stop