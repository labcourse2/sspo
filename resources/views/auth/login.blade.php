@extends('layouts.master')

@section('title', 'Login')

@section('content')

<div class="authenticate-form col-lg-5">
    <h2><strong>Log In</strong></h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>There were some problems with your input.</strong><br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form class="form-horizontal" method="POST" action="{{ url('/auth/login') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">*Email</label>

            <div class="col-sm-10">
                <input type="email" class="form-control" id="inputEmail3" name="email"
                       value="{{ old('email') }}" placeholder="Email" required>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">*Password</label>

            <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword3" name="password"
                       placeholder="Password" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Login</button>
            </div>
        </div>
    </form>
</div>

@stop