@extends('layouts.master')

@section('title', 'Home')

@section('content')
<h2 class="result-title">You query returned {{ $total }} results!</h2>

@if (!$products->isEmpty())
<div class="title">
    <h2>Products</h2>
</div>

<div class="row">
    @foreach ($products as $product)
    <div class="col-sm-3">
        <div class="post">
            <a href="/product/view/{{ $product->id }}">
                @if ($product->images->isEmpty())
                <img src="/repository/images/img_placeholder.png" alt="img" class="img-responsive">
                @else
                <img src="{{ $product->images->first()->url }}" alt="img" class="img-responsive">
                @endif
            </a>

            <div class="description">
                <h3><a href="/product/view/{{ $product->id }}">{{ $product->name }}</a></h3>
                @foreach ( $product->prices as $price)
                @if ($price->active)
                <h4><a href="/product/view/{{$product->id}}">{{ $price->price."$" }}</a></h4>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif


@if (!$categories->isEmpty())
<div class="title">
    <h2>Categories</h2>
</div>

<div class="row">
    @foreach ($categories as $cat)
    <div class="col-sm-3">
        <div class="post">
            <div class="description">
                <h3><a href="#">{{ $cat->name }}</a></h3>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif


@if (!$tags->isEmpty())
<div class="title">
    <h2>Tags</h2>
</div>

<div class="row">
    @foreach ($tags as $tag)
    <div class="col-sm-3">
        <div class="post">
            <div class="description">
                <h3><a href="#">{{ $tag->name }}</a></h3>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif


@if (!$companies->isEmpty())
<div class="title">
    <h2>Companies</h2>
</div>

<div class="row">
    @foreach ($companies as $comp)
    <div class="col-sm-3">
        <div class="post">
            <a href="/company/view/{{ $comp->id }}">
                @if (!$comp->logo_image)
                <img src="/repository/images/img_placeholder.png" alt="img" class="img-responsive">
                @else
                <img src="{{ $comp->logo_image_link }}" alt="img" class="img-responsive">
                @endif
            </a>

            <div class="description">
                <h3><a href="/company/view/{{ $comp->id }}">{{ $comp->name }}</a></h3>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif


@if (!$users->isEmpty())
<div class="title">
    <h2>Users</h2>
</div>

<div class="row">
    @foreach ($users as $user)
    <div class="col-sm-3">
        <div class="post">
            <a href="/profile/view/{{ $user->id }}">
                <img src="{{ $user->gravatar }}" alt="img" class="img-responsive">
            </a>

            <div class="description">
                <h3><a href="/profile/view/{{ $user->id }}">{{ $user->username }}</a></h3>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif



@stop

