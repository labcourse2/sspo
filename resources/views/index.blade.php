@extends('layouts.master')

@section('title', 'Home')

@section('content')

<!-- Featured -->
@if ($company)
<div class="title">
    <h2><a href="/company/view/{{$company->id}}">{{$company->name}}</a></h2>

    <div class="arrows">
        <div class="arrow-left"><a class="btn prev"></a></div>
        <div class="arrow-right"><a class="btn next"></a></div>
    </div>
</div>

<div class="row">
    <div id="owl-demo" class="owl-carousel owl-theme">
        @foreach($company->products as $product)
        <div class="item">

            <a href="#">
                @if ($product->images->isEmpty())
                <img src="/repository/images/img_placeholder.png" alt="img" class="img-responsive">
                @else
                <img src="{{ $product->images->first()->url }}" alt="img" class="img-responsive">
                @endif
            </a>

            <div class="description">
                <h3><a href="/product/view/{{ $product->id }}">{{ $product->name }}</a></h3>
                @foreach ( $product->prices as $price)
                @if ($price->active)
                <h4><a href="/product/view/{{$product->id}}">{{ $price->price."$" }}</a></h4>
                @endif
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif
<!-- End of Features -->

<!-- Categories & Products -->
@foreach ($categories as $category)
@if(!$category->products->isEmpty())
<div class="title">
        <h2><a href="{{ URL::route('product.index', ['category'=>$category->id]) }}">{{ $category->name }}</a></h2>
</div>

<div class="row">
    @foreach ($category->products->take(8) as $product)
    <div class="col-sm-3">
        <div class="post">
            <a href="/product/view/{{ $product->id }}">
                @if ($product->images->isEmpty())
                <img src="/repository/images/img_placeholder.png" alt="img" class="img-responsive">
                @else
                <img src="{{ $product->images->first()->url }}" alt="img" class="img-responsive">
                @endif
            </a>

            <div class="description">
                <h3><a href="/product/view/{{ $product->id }}">{{ $product->name }}</a></h3>
                @foreach ( $product->prices as $price)
                @if ($price->active)
                <h4><a href="/product/view/{{$product->id}}">{{ $price->price."$" }}</a></h4>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif

@endforeach
<!-- End of Categories & Products -->
@stop

