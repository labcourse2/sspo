@extends('layouts.master')

@section('title', 'Edit Profile')

@section('content')
<div class="profile-user-stuff"> <!-- .profile-user-stuff -->
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="profile-image">
                    <img src="{{$user->gravatar}}" alt="{{$user->username}}s Profile Picture">
                </div>
                <ul class="parent-list">
                    <li>
                        <a href="javascript:void(0)">Your account</a>
                        <ul class="grandchild-list">
                            <li>Username: {{$user->username}}</li>
                            <li>Email: {{$user->email}}</li>
                        </ul>
                    </li>
                    <li>
                        <i class="glyphicon glyphicon-plus"></i> <a href="#">Company</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>There were some problems with your input.</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="profile-personal-info">
                    <form id="profile_edit_form" action="{{url('profile/edit/')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $user->id }}">

                        <div class="form-group">
                            <label for="name">* Name </label>
                            <input type="text" class="form-control" id="name" name="f_name"
                                   value="{{ $user->profile->f_name }}">
                        </div>

                        <div class="form-group">
                            <label for="m_name">* Middle Name </label>
                            <input type="text" class="form-control" id="name" name="m_name"
                                   value="{{ $user->profile->m_name }}">
                        </div>

                        <div class="form-group">
                            <label for="surname">* Lastname </label>
                            <input type="text" class="form-control" id="surname" name="l_name"
                                   value="{{ $user->profile->l_name }}">
                        </div>

                        <fieldset>
                            <div class="form-group">
                                <label for="place">* City </label>
                                <select id="place" class="form-control" name="city">

                                    @foreach ($cities as $city)
                                    <option value="{{$city->id}}" {{ ($user->profile->city == $city)? 'selected' : ''}}>
                                        {{$city->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </fieldset>

                        <div class="form-group">
                            <label for="birthdate">* Birthdate: </label>
                            <input type="birthdate" class="form-control date" id="datepicker" name="birthdate"
                                   value="{{ $user->profile->birthdate }}">
                        </div>

                        <div class="form-group">
                            <label for="email">* Email: </label>
                            <input type="email" class="form-control" id="email" name="email"
                                   value="{{ $user->email }}">
                        </div>

                    </form>
                    <p><a href="profile_user_modify.html"
                          onclick="document.forms['profile_edit_form'].submit(); return false;"
                          class="btn btn-default"> Save Changes </a></p>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /.profile-user-stuff -->
@stop

