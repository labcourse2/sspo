@extends('layouts.master')

@section('title')
Welcome {{Auth::user()->profile->fullName}}

@stop

@section('content')

<div class="profile-user-stuff"> <!-- .profile-user-stuff -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="profile-image">
                            <img src="{{ $user->gravatar }}" alt="">
                            
                            
                        </div>
                        <ul class="parent-list">
                            <li>
                                <a href="javascript:void(0)">Your Account</a>
                                <ul class="grandchild-list">
                                    <li>Username: {{$user->username}}</li>
                                    <li>Email: {{$user->email}}</li>
                                </ul>
                            </li>

                            <li>
                                @if (Auth::user()->company()->first())
                                <a href="{{ url('/company/view') }}">View Company</a>
                                @else 
                                <i class="glyphicon glyphicon-plus"></i> <a href="{{ url('/company/create') }}">Company</a>
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-9">
                        <div class="profile-personal-info">
                            <form>
                              <div class="form-group">
                                <label for="name">Name </label>
                                <input type="text" class="form-control" id="name" placeholder="{{$user->profile->f_name}}" readonly>
                              </div>
                              <div class="form-group">
                                <label for="surname">Surname </label>
                                <input type="text" class="form-control" id="surname" placeholder="{{$user->profile->l_name}}" readonly>
                              </div>
                              <fieldset disabled>
                                <div class="form-group">
                                  <label for="place">City </label>
                                  <select id="place" class="form-control">
                                    @foreach ($cities as $city)
                                    <option value="{{ $city->id }}" {{ ($user->profile->city == $city)? 'selected' : ''}}>
                                        {{ $city->name}}
                                    </option>
                                    @endforeach
                                  </select>
                                </div>
                              </fieldset>
                              
                              <div class="form-group">
                                <label for="email">Email: </label>
                                <input type="email" class="form-control" id="email" placeholder="{{$user->email}}" readonly>
                              </div>
                              <div class="form-group">
                                <label for="date">Birthday: </label>
                                <input type="text" class="form-control" placeholder="{{$user->profile->birthdate}}" readonly>
                              </div>
                              @if(Auth::user()->id == $user->id)
                              <p><a href="{{ url('/profile/edit') }}" class="btn btn-default"> Edit </a></p>
                              @endif    
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /.profile-user-stuff -->
@stop