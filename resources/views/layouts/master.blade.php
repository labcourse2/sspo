<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>TOTUM.DEV - @yield('title')</title>

        <link rel="stylesheet" type="text/css" href="/css/vendor/demo.css"/>
        <!-- common styles -->
        <link rel="stylesheet" type="text/css" href="/css/vendor/dialog.css"/>
        <!-- individual effect -->
        <link rel="stylesheet" type="text/css" href="/css/vendor/dialog-ken.css"/>
        <link rel="stylesheet" type="text/css" href="/js/vendor/jquery-ui.css"/>

        <!-- OWL SLIDER -->
        <link rel="stylesheet" href="/css/vendor/owl.carousel.css">
        <link rel="stylesheet" href="/css/vendor/owl.transitions.css">
        <link rel="stylesheet" href="/css/vendor/owl.theme.css">

        <!-- Bootstrap -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">

        <!-- Style -->
        <link href="/css/style.css" rel="stylesheet">

        <script src="/js/vendor/modernizr.custom.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="wrapper">
            <header> <!-- header -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="logo">
                                <a href="/">
                                    <h1>Totum</h1>
                                    <p>Online Store</p>
                                    <img src="/repository/images/horizontal-line.png" alt="horizontal-line">
                                </a>

                            </div>
                        </div>
                        <div class="col-sm-offset-3 col-sm-7">
                            <div class="login-register-search">
                                <div class="login-register">
                                    @if (Auth::check())
                                    <ul>
                                        <li><a class="btn btn-default" href="{{ url('/auth/logout') }}"
                                               role="button">Logout</a></li>
                                                
                                        @if (Auth::user()->company()->first())
                                        <li><a href="/product/create" ><i class="glyphicon glyphicon-plus"></i> Add a product</a></li>
                                        <li><a href="/company/view" ><i class="glyphicon glyphicon-briefcase"></i>  My Company</a></li>
                                        @else
                                        <li><a href="/company/create" ><i class="glyphicon glyphicon-plus"></i> Create your Company</a></li>
                                        @endif

                                        <li>
                                            <div class="member">
                                                <div class="member-image">
                                                    <a href="{{ url('/profile/view') }}">
                                                        <img src="{{Auth::user()->gravatar}}" alt="Member img">
                                                    </a>
                                                </div>
                                                <div class="member-msg">
                                                    <p>Tung,<br>
                                                        <span class="current-member"><strong>{{Auth::user()->username}}</strong>.</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    @else
                                    <ul class="login">
                                        <li>
                                            <a href="{{ url('/auth/login') }}" class="btn btn-default">login</a>
                                        </li>
                                        <li><a href="{{ url('/auth/register') }}" role="button">register</a>
                                        </li>
                                    </ul>
                                    @endif
                                </div>

                                @if ($errors->has('query'))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->get('query') as $message)
                                        <li>{{ $message}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="search form-group">
                                    {!! Form::open(['route' => ['search.index'], 'method' => 'get']) !!}
                                    <input type="text" class="form-control" name="query" placeholder="Search">
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="navbar navbar-default">
                    </nav>

                </div>
            </header>
            <!-- /header-->

            <div class="body-content">
                <div class="container">
                    @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}
                    </div>
                    @endif
                    @yield('content')
                </div>
            </div>

            <footer> <!-- footer -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <h3>Category</h3>
                            <ul>
                                <li><a href="#">Popular</a></li>
                                <li><a href="#">Latest</a></li>
                                <li><a href="#">Most searched</a></li>
                                <li><a href="#">Computer</a></li>
                                <li><a href="#">Phones</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h3>Our Support</h3>
                            <ul>
                                <li><a href="#">SiteMap</a></li>
                                <li><a href="#">Search Terms</a></li>
                                <li><a href="#">Advanced Search</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Addresses</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h3>Social Channel</h3>
                            <ul>
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Google+</a></li>
                                <li><a href="#">Yahoo</a></li>
                                <li><a href="#">Msn</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h3>About us</h3>

                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                                Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                            </p>

                            <p>
                                <strong>Phone:</strong> 044 000 111;
                            </p>

                            <p>
                                <strong>e-mail:</strong> example@example.com
                            </p>
                        </div>
                    </div>
                </div>

                <div class="copyright">
                    <p>&copy; <strong>SSPO</strong>. All rights reserved - Designed by <strong>sspo.com</strong></p>
                </div>
            </footer>
            <!-- /footer -->
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/js/vendor/jquery-2.1.3.min.js"></script>
        <script src="/js/vendor/jquery-ui.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/js/vendor/bootstrap.min.js"></script>

        <!-- Login/Register dialogs -->
        <script src="/js/vendor/classie.js"></script>
        <script src="/js/vendor/dialogFx.js"></script>

        <!-- OWL Slider -->
        <script src="/js/vendor/owl.carousel.min.js"></script>
        <script src="/js/vendor/bxslider/jquery.bxslider.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/vendor/bxslider/jquery.bxslider.css"/>

        <!-- DATA-Table -->
        <script src="/js/vendor/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/vendor/jquery.dataTables.css"/>
        
        <script src="/js/vendor/bootstrap-rating.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/vendor/bootstrap-rating.css"/>
        
        <!-- Bootstrap TAG -->
        <script src="/js/vendor/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/vendor/bootstrap-tag/bootstrap-tagsinput.css"/>
        
        <!-- BootBox -->
        <script src="/js/vendor/bootbox/bootbox.min.js"></script>
        
        <!-- Validate Form -->
        <script src="/js/vendor/jquery.validate.min.js"></script>

        <!-- main.js -->
        <script src="/js/main.js"></script>
    </body>
</html>
