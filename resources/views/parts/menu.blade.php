<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Home <span class="sr-only"></span></a></li>
            @foreach ($categories as $cat)
            <li><a href="{{ URL::route('product.index', ['category'=>$cat->id]) }}">{{ $cat->name }}<span
                        class="sr-only"></span></a></li>
            @endforeach
            @if (Auth::check() && Auth::user()->isAdmin())
            <li class="dropdown">
                <a href="/admin" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true">
                    Admin
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/admin/accounts">Accounts</a></li>
                    <li><a href="/admin/company">Companies</a></li>
                    <li><a href="/city">Cities</a></li>
                    <li><a href="/category">Categories</a></li>
                    <li><a href="/role">Roles</a></li>
                </ul>
            </li>
            @endif
        </ul>
    </div>


    <!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
