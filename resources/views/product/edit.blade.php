@extends('layouts.master')
@section('title')
Edit Product
@stop
@section('content')

<div class="profile-user-stuff"> <!-- .profile-user-stuff -->
    <div class="container">
        <div class="row">
                
            <div class="col-sm-6 col-centered">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>There were some problems with your input.</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                
                <div class="profile-personal-info">
    <form id="validate-form" action="{{url('product/edit')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="company" value="{{ Auth::user()->company->id }}">

                        <input type="hidden" name="id" value="{{$product->id}}">

                        <div class="form-group">
                            <label for="name">* Product Name</label>
                            <input type="text" class="form-control" id="name"
                                   name="name" placeholder="Product Name" required value="{{ $product->name }}">
                        </div>

                        <div class="form-group">
                            <label for="price">* Price</label>
                            <div class="input-icon">
                                <input type="text" name="price" class="form-control" aria-label="Price" value="{{$product->getActivePriceAttributes()->price}}">
                                <span class="glyphicon glyphicon-euro" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sh_description">* Short description</label>
                            <textarea class="form-control" rows="3" name="sh_description" 
                                      placeholder="Product Description">{{$product->sh_description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="l_description">* Long Description</label>
                            <textarea class="form-control" rows="3" name="l_description" 
                                      placeholder="Product Description">{{$product->l_description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="name">* Product Tags</label>
                            <p>Separate tags using enter, tab or comma.</p>
                            <input type="text" class="form-control" id="tags"
                                   name="tags" placeholder="Tags" value="{{ $product->show_tags }}" required data-role="tagsinput">
                        </div>
                        <div class="form-group">
                            <label for="input-image">* Images</label>
                            {!! Form::file('images[]', array('multiple'=>true)) !!}

                            <ul class="prev-images">
                                @foreach ($product->images as $image)
                                <li>
                                    <div class="see-if-this-gets-removed">
                                        <a href="#" class="remove-img glyphicon glyphicon-remove"
                                           data-image="{{ $image->id }}"
                                           data-token="{{ csrf_token() }}" data-url="/image/{{ $image->id }}"
                                           onclick="remove_image(this)"></a>

                                        <div class="img-container">
                                            <img src="{{ $image->url }}" alt="Product"/>
                                        </div>

                                </li>
                                @endforeach
                            </ul>
                        </div>

                        <fieldset>
                            <div class="form-group">
                                <label for="category">* Choose Category</label>
                                <select id="category" class="form-control" name="category">
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}" {{($category->id == $product->category_id)? 'selected' : ''}}>
                                        {{$category->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </fieldset>
                        <button type="submit" class="btn btn-default">Save</button>
                        <a href="#" class="btn btn-danger delete-btn" data-toggle="modal" data-target="#deleteConfirm">Delete</a>
                    </form>

                    <script>

                        function remove_image (elem) {
                            var image_id = $(elem).data('image');
                            $.ajax({
                                url: $(elem).data('url'),
                                type: 'POST',
                                data: {
                                    _method: 'DELETE',
                                    _token: $(elem).data('token')
                                },
                                success: function (result) {
                                    console.log(result);
                                    $(elem).parent().remove()
                                }
                            });

                        }
                    </script>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete {{ $product->name }}</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you wanna delete "{{ $product->name }}" ? <br>
                                    This action can not be undone.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <a href="/product/delete/{{$product->id}}" class="btn btn-danger delete-btn">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop