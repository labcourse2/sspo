@extends('layouts.master')

@section('title', 'Home')

@section('content')

    <!-- Categories & Products -->
        @if(!$result->products->isEmpty())
            <div class="title">
                <h2><a href="{{ URL::route('product.index', [$result->getTable()=>$result->id]) }}">{{ $result->name }}</a></h2>
            </div>

            <div class="row">
                @foreach ($result->products as $product)
                    <div class="col-sm-3">
                        <div class="post">
                            <a href="/product/view/{{ $product->id }}">
                                @if ($product->images->isEmpty())
                                    <img src="/repository/images/img_placeholder.png" alt="img" class="img-responsive">
                                @else
                                    <img src="{{ $product->images->first()->url }}" alt="img" class="img-responsive">
                                @endif
                            </a>

                            <div class="description">
                                <h3><a href="/product/view/{{ $product->id }}">{{ $product->name }}</a></h3>
                                @foreach ( $product->prices as $price)
                                    @if ($price->active)
                                        <h4><a href="/product/view/{{$product->id}}">{{ $price->price."$" }}</a></h4>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif

@stop

