@extends('layouts.master')
@section('title')
Add new Product
@stop
@section('content')

<div class="profile-user-stuff"> <!-- .profile-user-stuff -->
    <div class="container">
        <div class="row">

            <div class="col-sm-6 col-centered">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>There were some problems with your input.</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="profile-personal-info">
                    <form id="validate-form" action="{{url('product/create')}}" method="POST"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="company" value="{{ Auth::user()->company->id }}">

                        <div class="form-group">
                            <label for="name">* Product Name</label>
                            <input type="text" class="form-control" id="name"
                                   name="name" placeholder="Product Name" required>
                        </div>

                        <div class="form-group">
                            <label for="price">* Price</label>

                            <div class="input-icon">
                                <input type="text" name="price" class="form-control" aria-label="Çmimi">
                                <span class="glyphicon glyphicon-euro" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sh_description">* Short description</label>
                            <textarea class="form-control" rows="3" name="sh_description"
                                      placeholder="Product Description"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="l_description">* Long Description</label>
                            <textarea class="form-control" rows="3" name="l_description"
                                      placeholder="Product Description"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="name">* Product Tags</label>
                            <p>Separate tags using enter, tab or comma.</p>
                            <input type="text" class="form-control" id="tags"
                                   name="tags" placeholder="Tags" value="" required data-role="tagsinput">
                        </div>

                        <div class="form-group">
                            <label for="input-image">* Images</label>
                            {!! Form::file('images[]', array('multiple'=>true)) !!}
                        </div>

                        <fieldset>
                            <div class="form-group">
                                <label for="category">* Chose Category</label>
                                <select id="category" class="form-control" name="category">
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}" {{($category->name == 'name')? 'selected' : ''}}>
                                        {{$category->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </fieldset>

                        <button type="submit" class="btn btn-default">Publish Product</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop