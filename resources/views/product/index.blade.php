@extends('layouts.master')
@section('title')
    {{ $product->name }}
@stop

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-7">
                <ul class="product-gallery">
                    @if ($product->images->isEmpty())
                        <li>
                            <img src="/repository/images/img_placeholder.png" alt="Product">
                        </li>
                    @endif
                    @foreach ($product->images as $image)
                        <li>
                            <img src="{{ $image->url }}" alt="Product">
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="col-sm-5">
                <div class="current-product-description">
                    <h2>{{ $product->name }}</h2>
                    <input type="hidden" class="rating" readonly="readonly" value="{{$product->ratings()->avg('rating')}}">
                    <p class="current-cost"><span class="money-type">$</span> {{ $product->getActivePriceAttributes()->price
                    }}</p>

                    <h3>Quick overview: </h3>

                    <p>
                        {{$product->sh_description}}
                    </p>
                </div>
                @if (Auth::check() && $product->ownedBy(Auth::user()))
                    <a href="/product/edit/{{ $product->id }}" class="btn btn-default"><span
                                class="glyphicon glyphicon-edit"></span> Edit product</a>
                @endif
                <div class="tags">
                    <h2>Tags</h2>
                    <ul>
                    @foreach($product->tags as $tag)
                        <li><a href="{{ URL::route('product.index', ['tag'=>$tag->id]) }}">{{$tag->name}}</a></li>
                    @endforeach    
                    </ul>
                </div>
            </div>
            <div class="col-sm-12">
                <h2>Long Description</h2>

                <p>{{$product->l_description}}</p>
            </div>
            <div class="col-sm-10">

                @if(Auth::check() && (!Auth::user()->ratingFor($product->id)))
                <h2>Review</h2>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>There were some problems with your input.</strong><br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="well well-sm">
                    <div class="row" id="post-review-box">
                        <!-- Rating Section -->
                            <div class="col-md-12">
                            {!! Form::open(['route' => ['rating.create', $product->id], 'method' => 'post']) !!}
                                <input type="hidden" name="rating" data-fractions="2" class="js-rating"/>
                                <textarea class="form-control" cols="50" id="new-review" name="comment"
                                      placeholder="Enter your review here..." rows="5"></textarea>
                                <button class="btn btn-default star-btn" type="submit">Publish Review</button>
                            {!! Form::close() !!}
                            </div>
                         <!-- End of Rating Section-->
                    </div>
                </div>
                @endif

                <div class="review-list">
                    <h2>Customer Reviews</h2>

                        <ul>
                            @foreach($product->ratings as $rating)
                                <li>
                                    @if (Auth::check() && (Auth::user()->id === $rating->user->id))
                                        {!! Form::open(['route' => ['rating.delete', $rating->id],
                                        'method' => 'delete']) !!}
                                        <button type="submit" title="Delete your comment and rating"
                                                    class="delete-confirm delete-comment glyphicon glyphicon-remove">
                                        </button>
                                        {!! Form::close() !!}
                                    @endif
                                    <span class="user">{{ $rating->user->username }}</span>
                                    <input type="hidden" class="rating" readonly="readonly" value="{{$rating->rating}}">
                                    <p>{{$rating->comment}}</p>
                                </li>
                            @endforeach
                        </ul>
                </div>
            </div>

            <div class="col-sm-2">
                <img src="/repository/images/ad_placeholder.jpg" alt="potencial commercial user"/>
            </div>
        </div>
    </div>
@stop