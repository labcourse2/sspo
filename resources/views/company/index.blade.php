@extends('layouts.master')
@section('title')
Welcome to {{$company->name}}
@stop

@section('content')
<div class="company-info"> <!-- .company-info -->

    <div class="company-title">
        <div class="title">
            <h2><strong>{{$company->name}}</strong></h2>
            <div class="clear-left"></div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-5">
            <div class="logo-image">
                
                <img src="{{getenv('UPLOADS_DIR').$company->logo_image}}" alt="Logo image">
            </div>
        </div>
        <div class="col-sm-7">
            @if(Auth::check() && $company->isOwner(Auth::user()))
            <a href="edit" class="btn btn-default company-btn">Edit Company</a>
            @endif
            <h3>
                Company's Info:
            </h3>

            <div class="row">
                <div class="col-sm-4">
                    <p><span>Company Name:</span></p>
                </div>
                <div class="col-sm-8">
                    <p>{{$company->name}}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <p><span>Company phone numbers</span></p>
                </div>
                <div class="col-sm-8">
                    <p>{{ $company->telephone }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <p><span>Company Address</span></p>
                </div>
                <div class="col-sm-8">
                    <p>{{$company->address}}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <p><span>E-mail:</span></p>
                </div>
                <div class="col-sm-8">
                    <p>{{$company->email}}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <p><span>Website:</span></p>
                </div>
                <div class="col-sm-8">
                    <a target="_blank" href="{{$company->web_address}}">{{$company->web_address}}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="company-description">
        <h3>Description</h3>

        <p>{!! $company->description !!}</p>
    </div>

</div> <!-- /.company-info -->

<!-- Categories & Products -->
@foreach ($categories as $category)
@if(!$category->productsFor($company)->isEmpty())
<div class="title">
    <h2><a href="{{ URL::route('product.index', ['category'=>$category->id]) }}">{{ $category->name }}</a></h2>
</div>
<div class="row">

    @foreach ($category->productsFor($company) as $product)
    <div class="col-sm-3">
        <div class="post">
            <a href="/product/view/{{ $product->id }}">
                @if ($product->images->isEmpty())
                <img src="/repository/images/img_placeholder.png" alt="img" class="img-responsive">
                @else
                <img src="{{ $product->images->first()->url }}" alt="img" class="img-responsive">
                @endif
            </a>

            <div class="description">
                <h3><a href="/product/view/{{ $product->id }}">{{ $product->name }}</a></h3>
                @foreach ( $product->prices as $price)
                @if ($price->active)
                <h4><a href="/product/view/{{$product->id}}">{{ $price->price."$" }}</a></h4>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif
@endforeach
<!-- End of Categories & Products -->
@stop