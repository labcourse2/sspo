@extends('layouts.master')
@section('title')
Edit Company
@stop
@section('content')

<div class="profile-user-stuff"> <!-- .profile-user-stuff -->
    <div class="container">

        <div class="row">

            <div class="col-sm-3">
                <div class="profile-image">
                    <img src="{{$user->gravatar}}" alt="{{$user->username}}s Profile Picture">
                </div>
                <ul class="parent-list">
                    <li>
                        <a href="javascript:void(0)">Your Account</a>
                        <ul class="grandchild-list">
                            <li>Username: {{$user->username}}</li>
                            <li>Email: {{$user->email}}</li>
                        </ul>
                    </li>
                    <li>
                        <i class="glyphicon glyphicon-plus"></i> <a href="#">Company</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>There were some problems with your input.</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="profile-personal-info">
                    <form id="validate-form" action="{{url('company/edit')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="company_id" value="{{ $company->id }}">

                        <div class="form-group">
                            <label for="name">* Company Name</label>
                            <input type="text" class="form-control" id="name" value="{{ $company->name }}"
                                   name="name" placeholder="Company Name" >
                        </div>
                        <div class="form-group">
                            <label for="name">* Company Address</label>
                            <input type="text" class="form-control" id="address" value="{{ $company->address }}"
                                   name="address" placeholder="Company Address" required >
                        </div>
                        <div class="form-group">
                            <label for="slogan">* Company slogan</label>
                            <input type="text" class="form-control" id="slogan"
                                   name="slogan" placeholder="Company Slogan" value="{{ $company->slogan }}" required>
                        </div>
                        <div class="form-group">
                            <label for="email">* Email</label>
                            <input type="email" class="form-control" placeholder="Email" name="email"
                                   value="{{ $company->email }}" required>
                        </div>

                        <div class="form-group">
                            <label for="number">* Number</label>
                            <input type="text" class="form-control"  placeholder="Number"
                                   name="telephone" value="{{ $company->telephone }}" required>
                        </div>

                        <div class="form-group">
                            <label for="logo_image">* Upload Logo Image</label>
                            <input type="file" id="logo_image" name="logo_image">
                        </div>

                        <div class="form-group">
                            <label for="web_address">* Website Url:</label>
                            <input type="text" class="form-control" name="web_address" value="{{ $company->web_address }}" required>
                        </div>
                        <fieldset>
                            <div class="form-group">
                                <label for="place">Location </label>
                                <select id="place" class="form-control" name="city">

                                    @foreach ($cities as $city)
                                    <option value="{{$city->id}}" {{($user->profile->city == $city)? 'selected' : ''}}>
                                        {{$city->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-image">* Description</label>
                            <textarea class="form-control" rows="3" name="description" placeholder="Place your description here">{{ $company->description }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-default">Save Changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop